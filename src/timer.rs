use core::{
    future::Future,
    marker::Unpin,
    ops::{
        Deref,
        DerefMut,
    },
    pin::Pin,
    task::{
        Context,
        Poll,
    },
};

use futures::ready;
use futures::prelude::*;

use cortex_m_async::flag::Flag;

use embedded_hal::timer::{
    CountDown,
    Periodic,
};

use crate::{
    hal::timer as hal,
    stm32::{
        interrupt,
        Interrupt,
        TIM1,
        TIM2,
        TIM3,
        TIM4,
    },
};

macro_rules! async_timer_interrupt {
    ($timer:ident, $reg:ident, $sys:ident) => {
        static $sys: Flag = Flag::new();

        #[allow(missing_docs)]
        #[interrupt]
        fn $timer() {
            unsafe { &*crate::stm32::$reg::ptr() }
                .sr
                .modify(|_, w| w.uif().clear_bit());
            $sys.wake();
        }
    };
}

async_timer_interrupt!(TIM1_UP, TIM1, TIM1_FLAG);
async_timer_interrupt!(TIM2, TIM2, TIM2_FLAG);
async_timer_interrupt!(TIM3, TIM3, TIM3_FLAG);
async_timer_interrupt!(TIM4, TIM4, TIM4_FLAG);

/// Async wrapper for an `embedded_hal` timer
pub struct AsyncTimer<T: TimerExt>(T);

impl<T: TimerExt> Deref for AsyncTimer<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: TimerExt> DerefMut for AsyncTimer<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Extension trait for `embedded_hal` timers to make them async
pub trait TimerExt: Sized {
    /// Wrap a timer to make it async
    fn into_async(self, nvic: &mut crate::stm32::NVIC) -> AsyncTimer<Self>;
    /// Enable the interrupt for th timer
    fn enable_interrupt(&mut self);
    /// Disable the interrupt for the timer
    fn disable_interrupt(&mut self);
    /// Get the waker/woken flag
    fn get_flag(&self) -> &'static Flag;
}

macro_rules! impl_ext {
    ($timer:ident, $int:ident, $flag:expr) => {
        impl TimerExt for hal::Timer<$timer> {
            fn into_async(mut self, nvic: &mut crate::stm32::NVIC) -> AsyncTimer<Self> {
                self.enable_interrupt();
                nvic.enable(Interrupt::$int);
                AsyncTimer(self)
            }
            fn enable_interrupt(&mut self) {
                self.listen(hal::Event::Update);
            }

            fn disable_interrupt(&mut self) {
                self.unlisten(hal::Event::Update);
            }

            fn get_flag(&self) -> &'static Flag {
                &$flag
            }
        }
    };
}

impl_ext!(TIM1, TIM1_UP, TIM1_FLAG);
impl_ext!(TIM2, TIM2, TIM2_FLAG);
impl_ext!(TIM3, TIM3, TIM3_FLAG);
impl_ext!(TIM4, TIM4, TIM4_FLAG);

impl<T: TimerExt> Drop for AsyncTimer<T> {
    fn drop(&mut self) {
        self.0.disable_interrupt();
    }
}

impl<T: TimerExt + CountDown> AsyncTimer<T> {
    /// Poll the timer to see if it's up yet
    pub fn poll_wait(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let this = unsafe { self.get_unchecked_mut() };

        if this.0.get_flag().check(cx.waker()) {
            Poll::Ready(())
        } else {
            Poll::Pending
        }
    }

    /// Return a future that will complete when the timer elapses
    pub fn wait(&mut self) -> Wait<'_, T>
    where
        T: Unpin,
    {
        Wait { inner: self }
    }

    /// Return a stream of timer events
    pub fn interval(&mut self) -> Interval<'_, T>
    where T: Unpin {
        Interval { inner: self }
    }
}

/// Future returned from [AsyncTimer::wait]
pub struct Wait<'t, T: TimerExt> {
    inner: &'t mut AsyncTimer<T>,
}

impl<'t, T> Future for Wait<'t, T>
where
    T: Periodic + CountDown + TimerExt + Unpin,
{
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let this = self.get_mut();
        Pin::new(&mut *this.inner).poll_wait(cx)
    }
}

pub struct Interval<'t, T: TimerExt> {
    inner: &'t mut AsyncTimer<T>,
}

impl<'t, T: TimerExt + CountDown + Periodic + Unpin> Stream for Interval<'t, T> {
    type Item = ();

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<()>> {
        let this = self.get_mut();
        ready!(Pin::new(&mut *this.inner).poll_wait(cx));
        Poll::Ready(Some(()))
    }
}

impl<T: TimerExt + CountDown + Unpin> Future for AsyncTimer<T> {
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        self.poll_wait(cx)
    }
}

impl<T: TimerExt + CountDown + Periodic + Unpin> Stream for AsyncTimer<T> {
    type Item = ();
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<()>> {
        ready!(self.poll_wait(cx));
        Poll::Ready(Some(()))
    }
}