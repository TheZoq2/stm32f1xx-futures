use core::{
    cell::RefCell,
    marker::PhantomData,
    task::Waker,
};

use cortex_m::interrupt::{
    self,
    Mutex,
};

use super::metal::AsyncUsart;

/// Struct holding the wakers for the USART interrupt.
pub struct UsartWakers<U> {
    read_waker: Option<Waker>,
    write_waker: Option<Waker>,
    flush_waker: Option<Waker>,
    _ph: PhantomData<U>,
}

macro_rules! waker {
    ($set_fn:ident, $wake_fn:ident, $field:ident, $usart:ident, $check_fn:ident, $enable_fn:ident) => {
        /// Set the waker for this operation
        pub fn $set_fn(&mut self, waker: &Waker) {
            if let Some(current_waker) = self.$field.as_mut() {
                if !waker.will_wake(current_waker) {
                    *current_waker = waker.clone();
                }
            } else {
                self.$field= Some(waker.clone());
            }
            $usart::$enable_fn( true);
        }

        /// Call the waker for this operation
        pub fn $wake_fn(&mut self) {
            if $usart::$check_fn() {
                if let Some(waker) = self.$field.take() {
                    $usart::$enable_fn(false);
                    waker.wake();
                }
            }
        }
    };
}

impl<U> UsartWakers<U>
where
    U: AsyncUsart,
{
    /// Create a new set of USART wakers
    pub const fn new() -> Self {
        UsartWakers {
            read_waker: None,
            write_waker: None,
            flush_waker: None,
            _ph: PhantomData,
        }
    }

    waker!(
        set_read,
        maybe_wake_read,
        read_waker,
        U,
        check_rxne,
        enable_rxne
    );
    waker!(
        set_write,
        maybe_wake_write,
        write_waker,
        U,
        check_txe,
        enable_txe
    );
    waker!(
        set_flush,
        maybe_wake_flush,
        flush_waker,
        U,
        check_tc,
        enable_tc
    );
}

/// Wrapper for [UsartWakers] to be used behind a static reference.
pub struct AsyncUsartSystem<U>(Mutex<RefCell<UsartWakers<U>>>);

impl<U: AsyncUsart> AsyncUsartSystem<U> {
    /// Initialize a new USART Waker System
    pub(crate) const fn new() -> Self {
        let wakers = UsartWakers::new();
        let ref_cell = RefCell::new(wakers);
        let mutex = Mutex::new(ref_cell);
        AsyncUsartSystem(mutex)
    }

    /// Modify the [UsartWakers]
    pub fn modify<R>(&self, f: impl FnOnce(&mut UsartWakers<U>) -> R) -> R {
        interrupt::free(move |cs| {
            let mut lock = self.0.borrow(cs).borrow_mut();
            f(&mut *lock)
        })
    }
}
