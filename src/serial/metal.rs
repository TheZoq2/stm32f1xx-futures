use crate::stm32::{
    interrupt,
    USART1,
    USART2,
    USART3,
};

use super::wakers::AsyncUsartSystem;

/// Low-level async USART trait
///
/// Contains all methods needed to manage both the wakers and interrupts.
pub trait AsyncUsart: Sized + 'static {
    /// Get a reference to the `AsyncUsartSystem` for this port
    fn get_system() -> &'static AsyncUsartSystem<Self>;

    /// Manage the "transmit complete" interrupt
    fn enable_tc(bit: bool);
    /// Check the "transmit complete" status
    fn check_tc() -> bool;

    /// Manage the "receive register not empty" interrupt
    fn enable_rxne(bit: bool);
    /// Check the "receive register not empty" status
    fn check_rxne() -> bool;

    /// Manage the "transmit register empty" interrupt
    fn enable_txe(bit: bool);
    /// Check the "transmit register empty" status
    fn check_txe() -> bool;
}

macro_rules! impl_async_interrupt {
    (f $usart:ident $enable_fn:ident $check_fn:ident : $status:ident, $enable:ident) => {
        fn $enable_fn(bit: bool) {
            unsafe { (*$usart::ptr()).cr1.modify(|_, w| w.$enable().bit(bit)); }
        }
        fn $check_fn() -> bool {
            unsafe { (*$usart::ptr()).sr.read().$status().bit_is_set() }
        }
    };
    ($usart:ident, $sys_name:ident) => {
        impl AsyncUsart for $usart {
            fn get_system() -> &'static AsyncUsartSystem<Self> {
                &$sys_name
            }
            impl_async_interrupt!(f $usart enable_txe check_txe : txe, txeie);
            impl_async_interrupt!(f $usart enable_rxne check_rxne : rxne, rxneie);
            impl_async_interrupt!(f $usart enable_tc check_tc : tc, tcie);
        }

        static $sys_name: AsyncUsartSystem<$usart> = AsyncUsartSystem::new();

        #[interrupt]
        /// USART Interrupt
        unsafe fn $usart() {
            $sys_name.modify(|sys| {
                sys.maybe_wake_read();
                sys.maybe_wake_write();
                sys.maybe_wake_flush();
            })
        }
    };
}

impl_async_interrupt!(USART1, U1_SYSTEM);
impl_async_interrupt!(USART2, U2_SYSTEM);
impl_async_interrupt!(USART3, U3_SYSTEM);
