pub use crate::{
    serial::SerialExt,
    timer::TimerExt,
};

#[doc(hidden)]
pub use stm32f1xx_hal::prelude::*;
