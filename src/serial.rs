/// Low-level implementation of interrupts
pub mod metal;

/// Wakers shared by interrupts and tasks
pub mod wakers;

mod hal {
    pub use crate::hal::serial::{
        Rx,
        Serial,
        Tx,
    };
    pub use embedded_hal::serial::{
        Read,
        Write,
    };
}

use core::{
    fmt::{
        self,
        Debug,
    },
    pin::Pin,
    task::{
        Context,
        Poll,
    },
};

use embrio::io::{
    Read,
    Write,
};

use hal::{
    Read as _,
    Serial,
    Write as _,
};

use metal::*;

/// Async Serial Writer
pub struct Tx<U> {
    inner: hal::Tx<U>,
    closed: bool,
}

/// Async Serial Reader
pub struct Rx<U>
where
    hal::Rx<U>: hal::Read<u8>,
{
    inner: hal::Rx<U>,
    last_err: Option<<hal::Rx<U> as hal::Read<u8>>::Error>,
}

unsafe impl<U> Sync for Rx<U>
where
    hal::Rx<U>: hal::Read<u8>,
    <hal::Rx<U> as hal::Read<u8>>::Error: Sync,
    U: Sync,
{
}

unsafe impl<U> Send for Rx<U>
where
    hal::Rx<U>: hal::Read<u8>,
    <hal::Rx<U> as hal::Read<u8>>::Error: Send,
    U: Send,
{
}

impl<U> Read for Rx<U>
where
    U: AsyncUsart + 'static,
    hal::Rx<U>: hal::Read<u8>,
    <hal::Rx<U> as hal::Read<u8>>::Error: Debug,
{
    type Error = <hal::Rx<U> as hal::Read<u8>>::Error;

    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<Result<usize, Self::Error>> {
        let this = unsafe { Pin::get_unchecked_mut(self) };
        if let Some(e) = this.last_err.take() {
            return Poll::Ready(Err(e));
        }
        let mut bytes_read = 0usize;
        loop {
            if bytes_read == buf.len() {
                return Poll::Ready(Ok(bytes_read));
            }
            match this.inner.read() {
                Ok(b) => {
                    buf[bytes_read] = b;
                    bytes_read += 1;
                }
                Err(nb::Error::WouldBlock) => {
                    if bytes_read == 0 {
                        U::get_system().modify(|ints| ints.set_read(cx.waker()));
                        return Poll::Pending;
                    } else {
                        return Poll::Ready(Ok(bytes_read));
                    }
                }
                Err(nb::Error::Other(e)) => {
                    this.last_err = Some(e);
                    return Poll::Ready(Ok(bytes_read));
                }
            }
        }
    }
}

/// Errors arising from `Tx::read`
pub enum TxError<E> {
    /// The [Tx] is closed
    Closed,
    /// Internal error from the USART
    Other(E),
}

impl<E> Debug for TxError<E>
where
    E: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TxError::Closed => write!(f, "Tx closed"),
            TxError::Other(e) => Debug::fmt(e, f),
        }
    }
}

impl<U> Write for Tx<U>
where
    U: AsyncUsart + 'static,
    hal::Tx<U>: hal::Write<u8>,
    <hal::Tx<U> as hal::Write<u8>>::Error: Debug,
{
    type Error = TxError<<hal::Tx<U> as hal::Write<u8>>::Error>;

    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, Self::Error>> {
        let this = unsafe { Pin::get_unchecked_mut(self) };
        if this.closed {
            return Poll::Ready(Err(TxError::Closed));
        }
        let buf_len = buf.len();
        if buf_len > 0 {
            let mut bytes_written = 0usize;
            loop {
                if bytes_written == buf_len {
                    return Poll::Ready(Ok(bytes_written));
                }
                match this.inner.write(buf[bytes_written]) {
                    Ok(()) => {
                        bytes_written += 1;
                    }
                    Err(nb::Error::WouldBlock) => {
                        if bytes_written != 0 {
                            return Poll::Ready(Ok(bytes_written));
                        } else {
                            U::get_system().modify(|ints| ints.set_write(cx.waker()));
                            return Poll::Pending;
                        }
                    }
                    Err(nb::Error::Other(e)) => return Poll::Ready(Err(TxError::Other(e))),
                }
            }
        } else {
            Poll::Ready(Ok(0))
        }
    }
    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        if self.as_ref().closed {
            return Poll::Ready(Err(TxError::Closed));
        }
        if U::check_txe() && U::check_tc() {
            Poll::Ready(Ok(()))
        } else {
            U::get_system().modify(|ints| ints.set_flush(cx.waker()));
            Poll::Pending
        }
    }
    fn poll_close(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        unsafe {
            self.get_unchecked_mut().closed = true;
        }
        Poll::Ready(Ok(()))
    }
}

/// Extension trait for existing [`Serial`] types from `stm32f1xx-hal`.
pub trait SerialExt: Sized
where
    hal::Rx<Self::Inner>: hal::Read<u8>,
    hal::Tx<Self::Inner>: hal::Write<u8>,
{
    /// Inner USART type
    type Inner;

    /// Split the port into an async write/read pair.
    fn split_async(self, nvic: &mut crate::stm32::NVIC) -> (Tx<Self::Inner>, Rx<Self::Inner>);
}

macro_rules! impl_ext {
    ($usart:ty, $int:expr) => {
        impl<P> SerialExt for Serial<$usart, P> {
            type Inner = $usart;
            fn split_async(
                self,
                nvic: &mut crate::stm32::NVIC,
            ) -> (Tx<Self::Inner>, Rx<Self::Inner>) {
                nvic.enable($int);
                let (tx, rx) = self.split();
                (
                    Tx {
                        inner: tx,
                        closed: false,
                    },
                    Rx {
                        inner: rx,
                        last_err: None,
                    },
                )
            }
        }
    };
}

impl_ext!(crate::stm32::USART1, crate::stm32::Interrupt::USART1);
impl_ext!(crate::stm32::USART2, crate::stm32::Interrupt::USART2);
impl_ext!(crate::stm32::USART3, crate::stm32::Interrupt::USART3);
